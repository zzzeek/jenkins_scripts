#!/bin/bash

# https://www.python.org/ftp/python/3.4.3/Python-3.4.3.tar.xz

WORK=/usr/local/src
DEST=/opt
VERSION=$1

set -x
set -e


cd ${WORK}

XZFILE=Python-${VERSION}.tar.xz
if [[ ! -f ${XZFILE} ]]; then
    wget https://www.python.org/ftp/python/${VERSION}/${XZFILE}
fi

tar -xf ${XZFILE}

SRC_DIR=Python-${VERSION}

OUTPUT_DIR=${DEST}/python-${VERSION}

sudo mkdir -p ${OUTPUT_DIR}

cd $SRC_DIR
./configure --prefix=${OUTPUT_DIR}
make
sudo make install
