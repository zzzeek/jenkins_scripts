#!/bin/bash

set -e
set -x

realpath ()
{
    echo $(cd $(dirname $1); pwd)
}

HERE=`realpath $0`
PWD=`pwd`

PYTHON_INTERP=$1
PYTHON_BIN_DIR=`dirname $PYTHON_INTERP`

PIP=${PYTHON_BIN_DIR}/pip

if [[ ! -f ${PIP} ]]; then

    GET_PIP=${PWD}/get-pip.py

    if [[ ! -f ${GET_PIP} ]]; then
        wget https://bootstrap.pypa.io/get-pip.py 
    fi

$PYTHON_INTERP ${GET_PIP}

fi

${PIP} install -r ${HERE}/requirements.txt
