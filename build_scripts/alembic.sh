#!/bin/bash

export PACKAGE=alembic
export LIB=alembic
export INSTALL_SQLALCHEMY=1
export TESTS=tests
export PY2K3KCOMPAT=1
export PYTESTARGS=tests/
HERE=`dirname $0`

${HERE}/build.sh
