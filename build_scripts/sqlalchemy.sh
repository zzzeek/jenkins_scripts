#!/bin/bash


export PACKAGE=sqlalchemy
export LIB=lib/sqlalchemy
export TESTS=test
export NOSERUNNER="sqla_nose.py -v"
export TWOTOTHREE="${PYTHON}/bin/python3 ${WORKSPACE}/sa2to3.py"


if [[ ! $MEMORYTESTS ]]; then
	export NOSEARGS="${NOSEARGS} -e memusage"
	export PYTESTARGS="${PYTESTARGS} -k not\ memusage"
fi


HERE=`dirname $0`

${HERE}/build.sh
