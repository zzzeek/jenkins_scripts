#!/bin/bash

export PACKAGE=mako
export LIB=mako
export TESTS=test
export PY2K3KCOMPAT=1

HERE=`dirname $0`

${HERE}/build.sh
