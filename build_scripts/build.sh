#!/bin/bash

# prerequisites:
# ${WORKSPACE}
# ${PYTHON}
# ${LIB}
# ${TESTS}

prerequisites ()
{
if [[ ! $WORKSPACE ]]; then
  echo "WORKSPACE not defined"
  exit -1
fi
if [[ ! $PYTHON ]]; then
  echo "PYTHON not defined"
  exit -1
fi
if [[ ! $LIB ]]; then
  echo "LIB not defined"
  exit -1
fi
if [[ ! $TESTS ]]; then
  echo "TESTS not defined"
  exit -1
fi

if [[ ! $PACKAGE ]]; then
  echo "PACKAGE not defined"
  exit -1
fi

}

globals()
{

if [[ ! $TESTRUNNER ]]; then

    if [[ -f tox.ini ]]; then
      TESTRUNNER=tox
    else
      TESTRUNNER=pytest
    fi
fi

if [[ ! $NOSERUNNER ]]; then
    NOSERUNNER="${PYTHON}/bin/nosetests -v"
fi

if [[ ! $PYTESTRUNNER ]]; then
    PYTESTRUNNER=${PYTHON}/bin/py.test
fi

if [[ ! $TOXRUNNER ]]; then
    TOXRUNNER=${PYTHON}/bin/tox
fi

if [[ ! $COVERPACKAGE ]]; then
    COVERPACKAGE=${PACKAGE}
fi

if [[ -z $DISABLE_COVERAGE ]]; then
    NOSECOVERAGE="--with-coverage --cover-package=${COVERPACKAGE}"
    PYTESTCOVERAGE="--cov=${COVERPACKAGE}"
    COVERAGE=1
fi

if [[ ! $TWOTOTHREE ]]; then
    TWOTOTHREE=${PYTHON}/bin/2to3
fi

if [[ $PY3K ]]; then
    PYTHON_CMD=python3
elif [[ $PYPY ]]; then
    PYTHON_CMD=pypy
    NOSECOVERAGE=""
    PYTESTCOVERAGE=""
else
    PYTHON_CMD=python
fi

VENV_DIR=${WORKSPACE}/.venv
VENV_PYTHON=${VENV_DIR}/bin/${PYTHON_CMD}

}

install_sqlalchemy ()
{
    echo "Installing SQLAlchemy dependency from a local checkout"
    if [[ -d sqlalchemy ]]; then
       cd sqlalchemy
       git pull origin master
    else
      git clone http://git.sqlalchemy.org/sqlalchemy.git
      cd sqlalchemy
    fi
#    if [[ $PY3K ]]; then
#       echo "Running sa2to3 for SQLAlchemy checkout"
#       ${VENV_PYTHON} sa2to3.py -w --no-diffs -w lib/
#    fi
    # this seems to run 2to3 and points the develop to build/.
    # not sure why that's not what it seemed to do earlier...
    ${VENV_PYTHON} setup.py develop
    cd ..
}

install_project ()
{
    if [[ $PY3K && ! $PY2K3KCOMPAT ]]; then
        echo "Running ${TWOTOTHREE} for package ${PACKAGE}"
        ${TWOTOTHREE} --no-diffs -w ${LIB} ${TESTS}
    fi
    ${VENV_PYTHON} setup.py ${SETUPARGS} develop
}

run_tests_nose ()
{
    # nose/pytest args are sometimes quoted strings w/ spaces.
    # 'read' syntax here from http://stackoverflow.com/a/10586169/34549
    # 'we have to use an array' from http://stackoverflow.com/a/4754015/34549
    IFS=' ' read -a nose_arg_array <<< "$NOSEARGS"

    # printf so that we display in the echo each arg individually with quotes...

    echo "Test command:"
    echo ${VENV_PYTHON} ${NOSERUNNER} ${NOSECOVERAGE} `printf '"%s" ' "${nose_arg_array[@]}"` ${SQLAARGS}
    ${VENV_PYTHON} ${NOSERUNNER} ${NOSECOVERAGE} "${nose_arg_array[@]}" ${SQLAARGS}
    STATUS=$?
    if [[ $COVERAGE ]]; then
        ${PYTHON}/bin/coverage xml --include=${LIB}/*
    fi
}

run_tests_pytest ()
{


    IFS=' ' read -a pytest_arg_array <<< "$PYTESTARGS"

    echo "Test command:"
    if [[ ! $PYTESTARGS ]]; then
        echo ${VENV_PYTHON} ${PYTESTRUNNER} ${PYTESTCOVERAGE} ${SQLAARGS}
        ${VENV_PYTHON} ${PYTESTRUNNER} ${PYTESTCOVERAGE} ${SQLAARGS}
    else
        echo ${VENV_PYTHON} ${PYTESTRUNNER} ${PYTESTCOVERAGE} `printf '"%s" ' "${pytest_arg_array[@]}"` ${SQLAARGS}
        ${VENV_PYTHON} ${PYTESTRUNNER} ${PYTESTCOVERAGE} "${pytest_arg_array[@]}" ${SQLAARGS}
    fi
    STATUS=$?
    if [[ $COVERAGE ]]; then
        ${PYTHON}/bin/coverage xml --include=${LIB}/*
    fi
}

run_tests_tox ()
{

    if [[ $PYPY ]]; then
        ENVIRON="-e lightweight"
    elif [[ $COVERAGE ]]; then
        ENVIRON="-e coverage"
    else
        ENVIRON="-e full"
    fi

    echo "Test command:"
    echo ${TOXRUNNER} ${ENVIRON} -- ${SQLAARGS}
    ${TOXRUNNER} ${ENVIRON} -- ${SQLAARGS}
    STATUS=$?
}

run_tests ()
{
    if [ $TESTRUNNER = 'nose' ]; then
      run_tests_nose
    fi

    if [ $TESTRUNNER = 'pytest' ]; then
      run_tests_pytest
    fi

    if [ $TESTRUNNER = 'tox' ]; then
      run_tests_tox
    fi
}
setup ()
{
    cd ${WORKSPACE}
    ${PYTHON}/bin/virtualenv --system-site-packages .venv
}

cleanup ()
{
    rm -fr .venv
}

prerequisites
globals
if [ $TESTRUNNER != 'tox' ]; then
  setup
  if [[ $INSTALL_SQLALCHEMY ]]; then
      install_sqlalchemy
  fi
  install_project
fi
run_tests
cleanup
exit ${STATUS}

