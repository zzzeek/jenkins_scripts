#!/bin/bash

echo "THIS IS NOT THE SCRIPT ANY MORE, USE ANSIBLE BUILD!!!!!!"

exit -1


BITBUCKET_GIT_BASE=https://bitbucket.org/zzzeek/
OPENSTACK_GIT_BASE=https://review.openstack.org/p/openstack/
GERRIT_BASE=https://gerrit.sqlalchemy.org/zzzeek/

# UPPER_CONSTRAINTS=https://git.openstack.org/cgit/openstack/requirements/plain/upper-constraints.txt
UPPER_CONSTRAINTS=https://git.openstack.org/cgit/openstack/requirements/plain/global-requirements.txt

realpath ()
{
    echo $(cd $(dirname $1); pwd)
}

HERE=`realpath $0`

prerequisites ()
{
    if [[ ! $WORKSPACE ]]; then
        echo "WORKSPACE not defined"
        exit -1
    fi
    # note: don't use $PYTHON !! tox or someone uses this.
    # b00m!
    if [[ ! $PYTHON_INTERP ]]; then
        echo "PYTHON_INTERP not defined"
        exit -1
    fi
    unset PYTHON
    PYTHON_BIN_DIR=`dirname $PYTHON_INTERP`

    if [[ ! $SQLALCHEMY_TAG ]]; then
        SQLALCHEMY_TAG=master
    fi

    if [[ ! $PIP ]]; then
        PIP=${PYTHON_BIN_DIR}/pip
    fi
    if [[ ! $TOX ]]; then
        TOX=${PYTHON_BIN_DIR}/tox
    fi

    PACKAGES=${WORKSPACE}/packages
}

setup ()
{
    cd $WORKSPACE
    if [[ ! -d ${PACKAGES} ]]; then
        mkdir ${PACKAGES}
    fi

    UPPER_CONSTRAINTS_FILE=${WORKSPACE}/upper_constraints.txt
    curl "$UPPER_CONSTRAINTS" --insecure --progress-bar --output ${UPPER_CONSTRAINTS_FILE}

    for keyword in SQLAlchemy alembic nova keystone neutron oslo.db ; do
        sed -i.tmp "s/^${keyword}.*//" ${UPPER_CONSTRAINTS_FILE};
    done

    wheel -r ${HERE}/requirements.txt
}

prepare_bitbucket_dependency ()
{
    DEP=$1
    VERSION=$2
    cd ${WORKSPACE}
    if [[ -d ${DEP} ]]; then
        cd ${DEP}
        _until_it_works git checkout ${VERSION}
        _until_it_works git pull origin ${VERSION}
    else
        _until_it_works git clone ${BITBUCKET_GIT_BASE}${DEP}.git
        cd ${DEP}

        if [[ ${VERSION} != 'master' ]]; then
            _until_it_works git checkout -b ${VERSION} origin/${VERSION}
        fi
    fi

    _build_sqla_overlay_project
}

# having problems w/ bitbucket (and likely in conjunction with
# my cablevision)
_until_it_works ()
{

  n=10
  until [ $n -eq 0 ]
   do
      $@ && break
      echo "Command failed; will retry $n more times"
      n=$[$n-1]
      sleep 5
   done

}


prepare_gerrit_dependency()
{
    DEP=$1
    TAG=$2
    cd ${WORKSPACE}
    if [[ ! -d ${DEP} ]]; then
        _until_it_works git clone ${BITBUCKET_GIT_BASE}${DEP}.git
    fi

    cd ${DEP}
    _until_it_works git fetch ${GERRIT_BASE}${DEP} $TAG && git checkout FETCH_HEAD

    _build_sqla_overlay_project
}

_build_sqla_overlay_project  () {
    PACKAGE=`${PYTHON_INTERP} setup.py --name`

    # blow away all the SQLAlchemy / alembic versions we dont want!
    rm -fr ${PACKAGES}/${PACKAGE}*.tar.gz
    rm -fr ${PACKAGES}/${PACKAGE}*.whl

    # then put ours in there
    ${PYTHON_INTERP} setup.py bdist_wheel --dist-dir ${PACKAGES}

}

prepare_openstack_project ()
{
    cd ${WORKSPACE}
    PROJECT=$1
    if [[ -d ${PROJECT} ]]; then
        cd ${PROJECT}
        git reset --hard
        git pull origin master
    else
        git clone ${OPENSTACK_GIT_BASE}${PROJECT}.git
        cd ${PROJECT}
    fi

    for patchfile in `ls ${HERE}/patches/${PROJECT}_*.patch`
    do
        patch -p1 < ${patchfile}
    done

    # SQLAlchemy upper bound should be only in the upper-constraints
    # at this point...
    sed -i.tmp 's/SQLAlchemy.*/SQLAlchemy>=0.7.9/' requirements.txt

    # nova has this, not running tests that need this...
    if [[ -f test-requirements.txt ]]; then
        sed -i.tmp 's/^libvirt-/# libvirt-/' test-requirements.txt
    fi
    if [[ -f test-requirements-py2.txt ]]; then
        sed -i.tmp 's/^libvirt-/# libvirt-/' test-requirements-py2.txt
    fi


    if [[ -f neutron/tests/functional/requirements.txt ]]; then
        wheel -r neutron/tests/functional/requirements.txt
    fi

    wheel -r requirements.txt
    if [[ -f test-requirements.txt ]]; then
        wheel -r test-requirements.txt
    fi

    if [[ -f test-requirements-py2.txt ]]; then
        wheel -r test-requirements-py2.txt
    fi

    if [[ $PROJECT == 'keystone' ]]; then
        wheel .[ldap]
        wheel .[memcache]
        wheel .[bandit]
        wheel .[mongodb]
    fi

    if [[ $PROJECT == 'oslo.db' ]]; then
        wheel .[test]
        wheel .[fixtures]
    fi
}

wheel ()
{
    # make a wheel or wheels, if not exists
    ${PYTHON_INTERP} -m pip wheel -c${UPPER_CONSTRAINTS_FILE} $1 $2 -w ${PACKAGES} -f ${PACKAGES}
}

run_project ()
{

    PROJECT=$1
    cd ${WORKSPACE}/${PROJECT}

    # 1. environment variables aren't enough to override every pip
    #    setting they have in tox.ini, like -U, so we just write a tox.ini file.
    # 2. for the actual tox, kill any downloading.
    #    you must use what we have in packages, that's it.
    # 3. don't want to install numpy, lxml etc. every time,
    #    these are systemwide.  it's OK!
    cat tox.ini > .temp_tox.ini
    cat >> .temp_tox.ini <<END

[testenv:sqla_py27]
${TOX_DEPS}
setenv =
    {[testenv]setenv}
    ${TOX_ENV}
install_command = pip install ${DASHC} --pre --find-links=file://${PACKAGES} --no-index {opts} {packages}
END

    # tox really doesn't care about the current python interpreter,
    # it goes looking in PATH.  So lets set the PATH to point to the
    # python we want here.
    PATH=${PYTHON_BIN_DIR}:$PATH ${TOX} -c .temp_tox.ini -v -r -e sqla_py27 $2 $3 $4 $5 $6 $7
}

set -e
set -x

prerequisites
setup

# download all requirements and openstack projects
prepare_openstack_project oslo.db
prepare_openstack_project nova
prepare_openstack_project neutron
prepare_openstack_project keystone

# set up sqlalchemy/alembic for specific versions/checkouts and
# replace them into downloaded requirements
if [[ $SQLALCHEMY_GERRIT_TAG ]]; then
    prepare_gerrit_dependency sqlalchemy ${SQLALCHEMY_GERRIT_TAG}
else
    prepare_bitbucket_dependency sqlalchemy ${SQLALCHEMY_TAG}
fi

if [[ $ALEMBIC_GERRIT_TAG ]]; then
    prepare_gerrit_dependency alembic ${ALEMBIC_GERRIT_TAG}
else
    prepare_bitbucket_dependency alembic master
fi

if [[ $DOGPILE_GERRIT_TAG ]]; then
    prepare_gerrit_dependency dogpile.cache ${DOGPILE_GERRIT_TAG}
else
    prepare_bitbucket_dependency dogpile.cache master
fi

# then run tox for the openstack projects, using
# existing downloaded packages

run_project oslo.db
run_project keystone -r keystone.tests.unit.*sql.*
run_project nova nova.tests.unit.db
TOX_DEPS="deps={[testenv:functional]deps}"; TOX_ENV="{[testenv:functional]setenv}"; run_project neutron neutron.tests.functional.db.test_migrations

