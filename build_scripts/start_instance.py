import boto.ec2
import argparse
import time
from mako.template import Template

def run_with_tag(tag, region, access_key, secret_key):
    conn = boto.ec2.connect_to_region(
            region,
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key
        )
    if conn is None:
        raise Exception("No connection for region: %s" % region)

    timeout = 600
    res = conn.get_all_reservations(filters={"tag-key": tag})
    if not res:
        raise Exception("no reservations for tag: %s" % tag)

    res = res[0]
    if not res.instances:
        raise Exception("no instances for reservation")

    inst = res.instances[0]
    inst.start()

    inst.update()
    while timeout > 0 and inst.state == 'pending':
        time.sleep(10)
        inst.update()
        timeout -= 10

    if timeout < 0:
        raise Exception("Instance %s timed out" % inst)

    return inst.ip_address


def write_file(source, dest, kw):
    with open(dest, "w") as file_:
        file_.write(Template(filename=source).
                render(**kw))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("tag", help="launch one instance with tag name")
    parser.add_argument("region", help="region")
    parser.add_argument("aws_access_key", help="access key")
    parser.add_argument("aws_secret_key", help="secret key")

    parser.add_argument("--tds_file", action="store_true", help="write tds file")
    options = parser.parse_args()

    ip_addr = run_with_tag(options.tag, options.region, options.aws_access_key, options.aws_secret_key)
    if options.tds_file:
        write_file("freetds.conf.mako", "/etc/freetds.conf", dict(ip_addr=ip_addr))
        write_file("odbc.ini.mako", "/etc/odbc.ini", {})


